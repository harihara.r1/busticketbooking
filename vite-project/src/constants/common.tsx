export const ERR_SOMETHING_WENT_WRONG = 'Something went wrong!';
export const ERR_SESSION_EXPIRED = 'Session expired';
export const ERR_UNAUTHORIZED = 'Access denied';
