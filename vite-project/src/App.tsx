/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { Route, Routes } from 'react-router-dom';
import './App.css';
import Login from './pages/login';
import PrivateRoute from './components/PrivateRoute/PrivateRoute';
import MainLayout from './layout/index';
import Home from './pages/home';
import Bookings from './pages/bookings';
import Trip from './pages/trip';

function App() {
  return (
    <Routes>
      <Route path="/login" element={<Login isSignIn={true} />} />
      <Route path="/register" element={<Login isSignIn={false} />} />
      <Route
        path="/"
        element={
          <PrivateRoute>
            <MainLayout />
          </PrivateRoute>
        }
      >
        <Route index element={<Home />}></Route>
        <Route path="trip/:trip_id" element={<Trip />} />
        <Route path="bookings" element={<Bookings />}></Route>
      </Route>
    </Routes>
  );
}

export default App;
