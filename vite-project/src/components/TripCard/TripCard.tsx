/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { Card, CardBody, CardText, CardHeader, Badge } from 'reactstrap';
import { AiFillClockCircle } from 'react-icons/ai';
import { useNavigate } from 'react-router-dom';

interface Props {
  _id: string;
  travels_name: string;
  dept_time: string;
  duration: number;
  seats_available: number;
  price: number;
  ac: boolean;
  isVertical?: boolean;
  from: string;
  to: string;
  date: string;
}
const TripCard = ({
  _id,
  travels_name,
  dept_time,
  duration,
  seats_available,
  price,
  ac,
  isVertical,
  from,
  to,
  date,
}: Props) => {
  const navigate = useNavigate();
  const onTripSelect = (trip_id: string) => {
    navigate(`/trip/${trip_id}?src=${from}&dst=${to}&date=${date}`);
  };
  return (
    <div className="trip-card">
      <div className="trip-details">
        <div>
          <Card className="rounded">
            <CardHeader>{travels_name}</CardHeader>
            <CardBody
              className={`d-flex justify-content-evenly align-items-center ${
                isVertical ? 'flex-column' : 'flex-row'
              }`}
            >
              <div className="my-2 mx-auto">
                <CardText className="fw-bolder fs-4">
                  <AiFillClockCircle />
                  {dept_time}
                </CardText>
                <CardText>Duration: {duration}h</CardText>
              </div>
              <div className="my-2 mx-auto">
                <div className="mb-4">
                  <CardText className="d-inline">Seats Available :</CardText>
                  <CardText className="d-inline fw-bolder fs-4">
                    {seats_available}
                  </CardText>
                </div>
                {ac ? (
                  <Badge color="info">AC</Badge>
                ) : (
                  <Badge color="warning">Non AC</Badge>
                )}
              </div>
              <div className="my-2 mx-auto">
                <CardText className="d-inline fs-6">INR </CardText>
                <CardText className="d-inline fw-bolder fs-4">{price}</CardText>
              </div>
            </CardBody>
          </Card>
        </div>
        {!isVertical && (
          <button className="rounded-bottom" onClick={() => onTripSelect(_id)}>
            Select Seats
          </button>
        )}
      </div>
    </div>
  );
};

export default TripCard;
