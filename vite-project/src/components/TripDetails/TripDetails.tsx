import TripCard from '../TripCard/TripCard';
import { Card, CardHeader, Button } from 'reactstrap';
import { Trip } from '../../types/trip';
import { formatDeptTime } from '../../utils/common';
interface Props {
  bookedSeats: number[];
  trip: Trip;
  handleBooking: () => void;
}
const TripDetails = ({ bookedSeats, trip, handleBooking }: Props) => {
  return (
    <div className="my-4 mx-auto  flex-1 bg-light-white border border-3 border-light max-w-35rem w-100">
      <TripCard
        isVertical
        from={trip.from}
        to={trip.to}
        date={new Date(trip.departure_dateTime).toISOString().split('T')[0]}
        _id={trip._id}
        travels_name="JET Line"
        dept_time={formatDeptTime(trip.departure_dateTime)}
        duration={trip.duration}
        seats_available={trip.seats_available}
        price={trip.price}
        ac={trip.ac}
      />
      <Card>
        {bookedSeats.length > 0 && [
          <CardHeader className="d-flex justify-content-between">
            <div>Your Seats {bookedSeats.join(',')}</div>
            <div>
              Price{' '}
              <p style={{ fontWeight: 'bolder', display: 'inline' }}>
                {bookedSeats.length * trip.price}
              </p>
            </div>
          </CardHeader>,

          <Button onClick={handleBooking}>Book Seats</Button>,
        ]}
      </Card>
    </div>
  );
};

export default TripDetails;
