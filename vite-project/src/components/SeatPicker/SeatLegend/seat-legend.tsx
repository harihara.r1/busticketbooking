import Seat from '../Seat/seat';

const SeatLegend = () => {
  return (
    <div className="flex-1">
      SEAT LEGEND
      <div className="d-flex justify-content-evenly align-items-center border-top">
        <Seat
          isAvailable
          isBooked={false}
          onSelect={() => {
            /* */
          }}
        />
        <p className=" m-auto">Available</p>
      </div>
      <div className="d-flex justify-content-evenly align-items-center border-top">
        <Seat
          isAvailable
          isBooked={true}
          onSelect={() => {
            /* */
          }}
        />
        <p className=" m-auto">Selected</p>
      </div>
      <div className="d-flex justify-content-evenly align-items-center border-top">
        <Seat
          isAvailable={false}
          isBooked={false}
          onSelect={() => {
            /* */
          }}
        />
        <p className=" m-auto">Booked</p>
      </div>
    </div>
  );
};

export default SeatLegend;
