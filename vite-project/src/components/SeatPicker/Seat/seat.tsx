import './seat.css';
const Seat = ({
  isBooked,
  isAvailable,
  seat_number,
  onSelect,
}: {
  isBooked: boolean;
  isAvailable: boolean;
  seat_number?: number;
  onSelect: (sno: number) => void;
}) => {
  return (
    <div
      className={`seat ${
        isAvailable
          ? isBooked
            ? `seat-booked`
            : `seat-not-booked`
          : 'seat-not-available'
      }`}
      onClick={() => {
        if (isAvailable && seat_number) onSelect(seat_number);
      }}
    >
      {seat_number}
    </div>
  );
};

export default Seat;
