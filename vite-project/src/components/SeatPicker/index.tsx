import { Col, Row } from 'reactstrap';
import Seat from './Seat/seat';
import SeatLegend from './SeatLegend/seat-legend';
interface Props {
  available_seat_numbers: number[];
  bookedSeats: number[];
  onSeatSelect: (seat: number) => void;
  handleBooking: (seats: number[]) => void;
}

const SeatPicker = (props: Props) => {
  const SEAT_NUMBERS: number[] = Array.from(
    { length: 30 },
    (_, index) => index + 1
  );

  return (
    <div className="d-flex my-4 mx-auto p-4 flex-1 max-w-35rem w-100 bg-light-white">
      <SeatLegend />
      <div className="m-auto bg-grey w-50 rounded">
        <Row>
          {SEAT_NUMBERS.map((i, index) => {
            return (
              <Col xs="4" key={Math.random() + index}>
                <Seat
                  isAvailable={props.available_seat_numbers.includes(i)}
                  isBooked={props.bookedSeats.includes(i)}
                  seat_number={i}
                  onSelect={props.onSeatSelect}
                />
              </Col>
            );
          })}
        </Row>
      </div>
    </div>
  );
};

export default SeatPicker;
