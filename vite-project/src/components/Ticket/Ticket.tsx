import { TbBus } from 'react-icons/tb';
import { formatDeptTime } from '../../utils/common';
import { MONTHS, DAYS } from '../../constants/trip';

interface Props {
  date: string;
  src: string;
  dst: string;
  travels_name: string;
  seat_numbers: number[];
}

const Ticket = ({ date, src, dst, travels_name, seat_numbers }: Props) => {
  const deptDate = new Date(date);
  const mm = MONTHS[deptDate.getMonth()];
  const day = DAYS[deptDate.getDay()];
  const yyyy = deptDate.getFullYear();
  const dd = deptDate.getDate();
  const time = formatDeptTime(date);
  return (
    <div
      className="d-flex flex-column  m-4 bg-white"
      style={{
        boxShadow: 'rgba(0, 0, 0, 0.16) 0px 1px 4px',
      }}
    >
      <div className="d-flex flex-column-reverse flex-sm-row justify-content-between  ">
        <div className="p-1 mx-4">
          <TbBus size="2rem" />
          <br />
          {dd} {day}
          <br />
          <p className="fw-bold">
            {mm} {yyyy}
          </p>
        </div>
        <div className="px-4 border-start d-flex text-start flex-column">
          <p className="fw-bold m-0">
            {src} - {dst}
          </p>
          <p className="fw-light m-0">{travels_name}</p>

          <p className="m-0  d-inline fw-bold"> Departure: {time}</p>
        </div>
      </div>
      <div className="d-flex flex-column justify-content-between">
        <div className="border-top p-2 text-white bg-gray-primary">
          Seat Number : {seat_numbers.join(',')}
        </div>
      </div>
    </div>
  );
};

export default Ticket;
