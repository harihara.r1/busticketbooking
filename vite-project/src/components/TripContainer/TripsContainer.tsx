import './TripsContainer.css';
import TripCard from '../TripCard/TripCard';
import { useSelector } from 'react-redux';
import { selectTrips } from '../../redux/tripSlice';
import { ImSad2 } from 'react-icons/im';
import { Spinner } from 'reactstrap';
import { formatDeptTime } from '../../utils/common';
import { useErrorBoundary } from 'react-error-boundary';
const TripsContainer = () => {
  const trips = useSelector(selectTrips);
  const { showBoundary } = useErrorBoundary();
  if (trips.loading) return <Spinner />;
  // Axios Error
  if (trips.err) {
    showBoundary(trips.err);
  }
  return (
    <div className="d-flex w-100 m-auto text-center p-4">
      {trips.trips ? (
        trips.trips.length > 0 ? (
          <div className="trips">
            {trips.trips.map((trip) => (
              <TripCard
                key={trip._id}
                _id={trip._id}
                from={trip.from}
                to={trip.to}
                date={
                  new Date(trip.departure_dateTime).toISOString().split('T')[0]
                }
                travels_name="JET Line"
                dept_time={formatDeptTime(trip.departure_dateTime)}
                duration={trip.duration}
                price={trip.price}
                seats_available={trip.seats_available}
                ac={trip.ac}
              />
            ))}
          </div>
        ) : (
          <div className="mx-auto">
            <ImSad2 size="3rem" />
            <div style={{ fontSize: 'larger', fontWeight: 'bolder' }}>
              Sorry No Trips Found!
            </div>
          </div>
        )
      ) : null}
    </div>
  );
};

export default TripsContainer;
