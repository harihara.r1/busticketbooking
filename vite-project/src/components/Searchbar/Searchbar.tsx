import { FC, FormEvent, useState } from 'react';
import { Button, Form, FormGroup, Input, Row, Col } from 'reactstrap';
import { useDispatch } from 'react-redux';
import { searchTrips } from '../../redux/tripSlice';
import { AppDispatch } from '../../redux/store';
import { showErrorModal } from '../../utils/common';
import { CITIES, ERR_SAME_SRC_DST } from '../../constants/home';

const SearchBar: FC = () => {
  const dispatch = useDispatch<AppDispatch>();
  const [from, setFrom] = useState(CITIES[0]);
  const [to, setTo] = useState(CITIES[1]);
  const [date, setDate] = useState(new Date().toISOString().split('T')[0]);

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    if (from === to) {
      showErrorModal(ERR_SAME_SRC_DST);
      return;
    }
    void dispatch(searchTrips({ src: from, dst: to, date: date }));
  };
  return (
    <div className="p-4">
      <Form onSubmit={handleSubmit}>
        <Row className="justify-content-center align-items-center">
          <Col md={3}>
            <FormGroup>
              <Input
                placeholder="Source"
                type="select"
                name="from"
                id="fromInput"
                value={from}
                required
                onChange={(e) => setFrom(e.target.value)}
              >
                {CITIES.map((item, idx) => (
                  <option key={idx + Math.random()} value={item}>
                    {item}
                  </option>
                ))}
              </Input>
            </FormGroup>
          </Col>
          <Col md={3} key={2}>
            <FormGroup>
              <Input
                placeholder="Destination"
                type="select"
                name="to"
                id="toInput"
                value={to}
                required
                onChange={(e) => setTo(e.target.value)}
              >
                {CITIES.map((item, idx) => (
                  <option key={idx + Math.random()} value={item}>
                    {item}
                  </option>
                ))}
              </Input>
            </FormGroup>
          </Col>
          <Col md={3} key={3}>
            <FormGroup>
              <Input
                min={date}
                type="date"
                name="date"
                id="dateInput"
                value={date}
                onChange={(e) => setDate(e.target.value)}
              />
            </FormGroup>
          </Col>
        </Row>
        <Button color="primary" type="submit">
          Search
        </Button>
      </Form>
    </div>
  );
};

export default SearchBar;
