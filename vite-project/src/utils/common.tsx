import Swal from 'sweetalert2';
export const formatDeptTime = (date_time: string) => {
  const date = new Date(date_time);
  return date.toLocaleTimeString(navigator.language, {
    hour: '2-digit',
    minute: '2-digit',
  });
};

export const showLoadingModal = async () => {
  await Swal.fire({
    title: 'Loading',
    html: 'Please wait...',
    allowOutsideClick: false,
    showConfirmButton: false,
    willOpen: () => {
      Swal.showLoading();
    },
  });
};

export const showErrorModal = (errorMessage: string) => {
  void Swal.fire({
    icon: 'error',
    title: 'Error',
    text: errorMessage,
  });
};
