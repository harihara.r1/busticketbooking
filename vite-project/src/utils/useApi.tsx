import { useState, useEffect } from 'react';
import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { MyAxiosError } from '../types/axios';

// Type for the API response data
type ApiResponse<T> = {
  data: T | null;
  loading: boolean;
  error: AxiosError | null;
};

// Custom hook for making API calls using Axios
function useApi<T>(url: string, config?: AxiosRequestConfig): ApiResponse<T> {
  const [data, setData] = useState<T | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<AxiosError | null>(null);
  useEffect(() => {
    // Function to make the API call
    const fetchData = async () => {
      try {
        const response: AxiosResponse<T> = await axios.get(url, config);
        setData(response.data);
      } catch (err) {
        const myErr = err as MyAxiosError;
        myErr.message = myErr.response.data.message;
        setError(myErr);
      } finally {
        setLoading(false);
      }
    };

    void fetchData(); // Call the function when the component mounts or when the URL or config changes
  }, [url, config]);

  return { data, loading, error };
}

export default useApi;
