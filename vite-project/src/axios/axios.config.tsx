import axios, { AxiosError } from 'axios';

axios.defaults.baseURL = 'http://localhost:3000';
axios.interceptors.request.use(function (config) {
  const accessToken = sessionStorage.getItem('accessToken');
  if (config.url !== 'login' && config.url !== 'register') {
    config.headers['Authorization'] = `Bearer ${accessToken || ''}`;
  }
  return config;
});
axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error: AxiosError) {
    if (error?.response?.status === 401) {
      sessionStorage.clear();
    }
    return Promise.reject(error);
  }
);
export default axios;
