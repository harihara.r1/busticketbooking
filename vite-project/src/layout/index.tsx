/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import React, { useEffect } from 'react';
import bgImg from '../assets/home.png';
import Header from './header';
import { Outlet } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getUserInfo, selectUser, clearUserInfo } from '../redux/userSlice';
import { Spinner } from 'reactstrap';
import { AppDispatch } from '../redux/store';
import {
  FallbackProps,
  useErrorBoundary,
  withErrorBoundary,
} from 'react-error-boundary';
import { clearTrips } from '../redux/tripSlice';
import ErrorPage from '../pages/error';
import authErrImg from '../assets/unauthorized.png';
const MainLayout = () => {
  const dispatch = useDispatch<AppDispatch>();
  const user = useSelector(selectUser);
  const { showBoundary } = useErrorBoundary();

  useEffect(() => {
    if (user.email === '' || user.name === '') void dispatch(getUserInfo());

    return () => {
      dispatch(clearUserInfo());
      dispatch(clearTrips());
    };
  }, []);

  // Fetching user details from accessToken
  if (user.loading) return <Spinner color="primary" />;
  // Error in hitting getUserInfo API
  if (user.error) {
    showBoundary(user.err);
  }
  return (
    <React.Fragment>
      <div className="w-100 mb-16" style={{ marginBottom: '56px' }}>
        <Header />
      </div>
      <div
        className="object-fit-cover"
        style={{ flex: 1, width: '100%', backgroundImage: `url(${bgImg})` }}
      >
        <Outlet />
      </div>
    </React.Fragment>
  );
};

const MainLayoutWithErrorBoundary = withErrorBoundary(MainLayout, {
  fallbackRender: ({ error, resetErrorBoundary }: FallbackProps) => {
    return (
      <ErrorPage
        error={error}
        title="Bad Auth"
        redirectBtnTitle="Login"
        redirectPath="/login"
        img={authErrImg}
        resetErrorBoundary={resetErrorBoundary}
      />
    );
  },
});
export default MainLayoutWithErrorBoundary;
