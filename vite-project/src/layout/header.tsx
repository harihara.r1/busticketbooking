import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Button,
} from 'reactstrap';
import { NavLink, useNavigate } from 'react-router-dom';

const SimpleNavbar: React.FC = () => {
  const [isOpen, setIsOpen] = useState(false);
  const navigate = useNavigate();
  const toggle = () => setIsOpen(!isOpen);
  const logoutHander = () => {
    window.sessionStorage.clear();
    navigate('/login');
  };
  return (
    <div>
      <Navbar color="primary" dark fixed="top" expand="md">
        <NavbarBrand href="/">get-bus</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="me-auto" navbar>
            <NavItem>
              <NavLink to="/" className="text-white text-decoration-none mx-4">
                Home
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                to="/bookings"
                className="text-white text-decoration-none"
              >
                Bookings
              </NavLink>
            </NavItem>
          </Nav>
          <Button color="primary" onClick={logoutHander}>
            Logout
          </Button>
        </Collapse>
      </Navbar>
    </div>
  );
};

export default SimpleNavbar;
