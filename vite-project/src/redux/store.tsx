import { configureStore } from '@reduxjs/toolkit';
import userReducer from './userSlice';
import tripReducer from './tripSlice';
export const store = configureStore({
  reducer: {
    user: userReducer,
    trip: tripReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
