import { createSlice, createAsyncThunk, AsyncThunk } from '@reduxjs/toolkit';
import { UserInfoResponse } from '../types/auth';
import { Slice } from '../types/common';
import axios, { AxiosError } from 'axios';
import { MyAxiosError } from '../types/axios';

type UserSlice = UserInfoResponse & Slice;
const initialState: UserSlice & { err: AxiosError | null } = {
  name: '',
  email: '',
  loading: false,
  error: false,
  err: null,
};

export const getUserInfo: AsyncThunk<UserInfoResponse, void, any> =
  createAsyncThunk('user/get-info', async () => {
    try {
      const response = await axios.get<UserInfoResponse>('user/get-info');
      return response.data;
    } catch (error) {
      const err = error as MyAxiosError;
      err.message = err.response?.data.message;
      throw err;
    }
  });

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    clearUserInfo: (state) => {
      state.email = '';
      state.err = null;
      state.error = false;
      state.loading = false;
      state.name = '';
    },
    updateUserInfo: (
      state,
      action: { payload: UserSlice & { err: AxiosError | null } }
    ) => {
      state.name = action.payload.name;
      state.email = action.payload.email;
      state.loading = action.payload.loading;
      state.error = action.payload.error;
      state.err = action.payload.err;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(getUserInfo.pending, (state) => {
        state.loading = true;
        state.error = false;
        state.err = null;
        state.name = '';
        state.email = '';
      })
      .addCase(getUserInfo.fulfilled, (state, action) => {
        state.loading = false;
        state.error = false;
        state.err = null;
        state.name = action.payload.name;
        state.email = action.payload.email;
      })
      .addCase(getUserInfo.rejected, (state, action) => {
        state.name = '';
        state.email = '';
        state.loading = false;
        state.error = true;
        state.err = action.error as AxiosError;
      });
  },
});
export const { clearUserInfo, updateUserInfo } = userSlice.actions;
export const selectUser = (state: {
  user: UserSlice & { err: AxiosError | null };
}) => state.user;
export default userSlice.reducer;
