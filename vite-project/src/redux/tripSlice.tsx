import { createSlice, createAsyncThunk, AsyncThunk } from '@reduxjs/toolkit';
import { TripsResponse } from '../types/trip';
import { Slice } from '../types/common';
import axios, { AxiosError } from 'axios';
import { MyAxiosError } from '../types/axios';
type TripSlice = TripsResponse & Slice;
const initialState: TripSlice & { status?: string; err?: null | AxiosError } = {
  trips: null,
  loading: false,
  error: false,
  status: 'idle',
  err: null,
};

export const searchTrips: AsyncThunk<
  TripsResponse,
  { src: string; dst: string; date: string },
  any
> = createAsyncThunk('trip/search-trip', async ({ src, dst, date }) => {
  try {
    const response = await axios.get<TripsResponse>(
      `/trip?src=${src}&dst=${dst}&date=${date}`
    );
    return response.data;
  } catch (error) {
    const err = error as MyAxiosError;
    err.message = err.response?.data.message;
    throw err;
  }
});

export const tripSlice = createSlice({
  name: 'trip',
  initialState,
  reducers: {
    clearTrips: (state) => {
      state.status = 'idle';
      state.trips = null;
      state.loading = false;
      state.error = false;
      state.err = null;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(searchTrips.pending, (state) => {
        state.status = 'loading';
        state.loading = true;
        state.trips = null;
        state.error = false;
        state.err = null;
      })
      .addCase(searchTrips.fulfilled, (state, action) => {
        state.err = null;
        state.status = 'succeeded';
        state.loading = false;
        state.trips = action.payload.trips;
        state.error = false;
      })
      .addCase(searchTrips.rejected, (state, action) => {
        state.status = 'failed';
        state.loading = false;
        state.trips = [];
        state.error = true;
        state.err = action.error as AxiosError;
      });
  },
});

export const { clearTrips } = tripSlice.actions;
export const selectTrips = (state: {
  trip: TripSlice & { status?: string; err: null | AxiosError };
}) => state.trip;

export default tripSlice.reducer;
