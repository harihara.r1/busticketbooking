// Booking
export type Booking = {
  _id: string;
  trip_id: string;

  count: number;
  booked_seat_numbers: number[];
  total_price: number;
  booked_date: string;
};
export type NewBooking = {
  booking: Booking;
};

type BookingList = {
  _id: string;
  count: number;
  booked_seat_numbers: number[];
  total_price: number;
  booked_date: string;
  trip: {
    departure_dateTime: string;
    duration: number;
    from: string;
    to: string;
  };
  agency: {
    name: string;
  };
};
export type BookingListResponse = {
  bookings: BookingList[];
};
