// Trip
export type Trip = {
  _id: string;
  travels_id: string;
  ac: boolean;
  available_seat_numbers: number[];
  seats_available: number;
  total_seats: number;
  from: string;
  to: string;
  departure_dateTime: string;
  duration: number;
  price: number;
};
// Search trips response
export type TripsResponse = {
  trips: Trip[] | null;
};
