// Login Form data
export type LoginForm = {
  username: string;
  email: string;
  password: string;
  phone: string;
  address: string;
};
// User Login
export type UserLogin = {
  email: string;
  password: string;
};
export type UserRegister = UserLogin & {
  name: string;
};
// Agency Register
export type AgencyRegister = UserRegister & {
  phone: string;
  address: string;
};
// Login response
export type AuthResponse = {
  accessToken: string;
  email: string;
  name: string;
};
export function isAuthResponse(obj: any): obj is AuthResponse {
  return 'accessToken' in obj;
}
// Get User Info response
export type UserInfoResponse = {
  name: string;
  email: string;
};
