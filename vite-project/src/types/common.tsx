// Types
export type Slice = {
  loading: boolean;
  error: boolean;
};
