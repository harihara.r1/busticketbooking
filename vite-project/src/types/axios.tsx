import { AxiosError } from 'axios';

export type ServerErrorResponse = {
  message: string;
  status: string;
  error: {
    statusCode: number;
  };
};
export type MyAxiosError = AxiosError & {
  response: {
    data: ServerErrorResponse;
  };
};
export const isMyAxiosError = (obj: any): obj is MyAxiosError => {
  return 'message' in obj;
};
