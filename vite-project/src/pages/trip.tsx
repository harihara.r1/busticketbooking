/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { NewBooking } from '../types/booking';
import { useNavigate, useParams, useSearchParams } from 'react-router-dom';
import { searchTrips, selectTrips } from '../redux/tripSlice';
import { ServerErrorResponse } from '../types/axios';
import SeatPicker from '../components/SeatPicker';
import TripDetails from '../components/TripDetails/TripDetails';
import { showLoadingModal, showErrorModal } from '../utils/common';
import Swal from 'sweetalert2';
import { AppDispatch } from '../redux/store';
import { Spinner } from 'reactstrap';
import {
  FallbackProps,
  useErrorBoundary,
  withErrorBoundary,
} from 'react-error-boundary';
import { Trip } from '../types/trip';
import ErrorPage from './error';
import tripImgErr from '../assets/network.png';
import { ERR_UNAUTHORIZED, ERR_SESSION_EXPIRED } from '../constants/common';

const TripComponent = () => {
  const { showBoundary } = useErrorBoundary();
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();
  const { trip_id } = useParams<{ trip_id: string }>();
  const [searchParams] = useSearchParams();
  const trips = useSelector(selectTrips);
  const [bookedSeats, setBookedSeats] = useState<number[]>([]);
  let tripDetails: Trip | undefined;

  const onSeatSelect = (sno: number) => {
    let prevBookedSeats = [...bookedSeats];
    if (prevBookedSeats.includes(sno)) {
      prevBookedSeats = prevBookedSeats.filter((seat) => seat !== sno);
    } else {
      prevBookedSeats = [...prevBookedSeats, sno];
    }
    setBookedSeats(prevBookedSeats);
  };
  const handleBooking = async () => {
    if (tripDetails) {
      const newBooking = {
        trip_id,
        count: bookedSeats.length,
        seat_numbers: bookedSeats,
        total_price: bookedSeats.length * tripDetails.price,
      };
      void showLoadingModal();
      const response = await axios.post<NewBooking & ServerErrorResponse>(
        '/booking',
        newBooking
      );
      void Swal.close();
      if (response.data.booking) {
        navigate(`/bookings`, { replace: true });
      } else showErrorModal(response.data.message);
    }
  };
  useEffect(() => {
    const src = searchParams.get('src') || '',
      dst = searchParams.get('dst') || '',
      date = searchParams.get('date') || '';
    if (!trip_id || !src || !dst || !date) {
      showBoundary(new Error('URL not found'));
      return;
    }
    if (trips.status === 'idle') {
      void dispatch(searchTrips({ src: src, dst: dst, date: date }));
    }
  }, []);

  if (trips.status === 'loading') return <Spinner />;
  if (trips.err) {
    showBoundary(trips.err);
  }
  if (trips.status === 'succeeded') {
    tripDetails = trips.trips?.find((trip) => trip._id === trip_id);
    if (!tripDetails) {
      showBoundary(new Error('Requested trip not found'));
      return;
    }
    return (
      <div className="d-flex flex-column flex-sm-row">
        <SeatPicker
          available_seat_numbers={tripDetails.available_seat_numbers}
          bookedSeats={bookedSeats}
          onSeatSelect={onSeatSelect}
          handleBooking={handleBooking}
        />

        <TripDetails
          trip={tripDetails}
          bookedSeats={bookedSeats}
          handleBooking={handleBooking}
        />
      </div>
    );
  }
};
const TripWithErrorBoundary = withErrorBoundary(TripComponent, {
  fallbackRender: (props: FallbackProps) => {
    if (
      props.error.message === ERR_UNAUTHORIZED ||
      props.error.message === ERR_SESSION_EXPIRED
    ) {
      // Bubble to Auth Err boundary
      throw props.error;
    }
    return (
      <ErrorPage
        error={props.error}
        redirectBtnTitle="Go Home"
        redirectPath="/"
        img={tripImgErr}
        resetErrorBoundary={props.resetErrorBoundary}
      />
    );
  },
});
export default TripWithErrorBoundary;
