/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { Spinner } from 'reactstrap';
import Ticket from '../components/Ticket/Ticket';
import { BookingListResponse } from '../types/booking';
import useApi from '../utils/useApi';
import {
  withErrorBoundary,
  FallbackProps,
  useErrorBoundary,
} from 'react-error-boundary';
import { ERR_UNAUTHORIZED, ERR_SESSION_EXPIRED } from '../constants/common';
import ErrorPage from './error';
import bookingImgErr from '../assets/network.png';

const Bookings = () => {
  const { showBoundary } = useErrorBoundary();
  const { data, loading, error } = useApi<BookingListResponse>('/booking');
  const bookings = data?.bookings || [];
  if (error) {
    showBoundary(error);
  }
  return (
    <div className="d-flex flex-wrap">
      {loading ? (
        <Spinner />
      ) : bookings.length > 0 ? (
        bookings.map((ticket) => {
          return (
            <Ticket
              key={ticket._id}
              travels_name={ticket.agency.name}
              date={ticket.trip.departure_dateTime}
              src={ticket.trip.from}
              dst={ticket.trip.to}
              seat_numbers={ticket.booked_seat_numbers}
            />
          );
        })
      ) : (
        'No bookings'
      )}
    </div>
  );
};
const BookingWithErrorBoundary = withErrorBoundary(Bookings, {
  fallbackRender: (props: FallbackProps) => {
    if (
      props.error.message === ERR_UNAUTHORIZED ||
      props.error.message === ERR_SESSION_EXPIRED
    ) {
      // Bubble to Auth Err boundary
      throw props.error;
    }
    return (
      <ErrorPage
        error={props.error}
        redirectBtnTitle="Go Home"
        redirectPath="/"
        img={bookingImgErr}
        resetErrorBoundary={props.resetErrorBoundary}
      />
    );
  },
});
export default BookingWithErrorBoundary;
