/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { useEffect } from 'react';
import { ChangeEvent, FormEvent, useState } from 'react';
import {
  Form,
  FormGroup,
  Input,
  Button,
  Card,
  ButtonGroup,
  CardBody,
  CardText,
  Alert,
} from 'reactstrap';
import { UserRegister, AgencyRegister, AuthResponse } from '../types/auth';
import { useNavigate, Navigate } from 'react-router-dom';
import axios from 'axios';
import { MyAxiosError } from '../types/axios';
import { LoginForm } from '../types/auth';
import { useDispatch } from 'react-redux';
import { updateUserInfo } from '../redux/userSlice';

const Login: React.FC<{ isSignIn: boolean }> = ({ isSignIn }) => {
  const ACCESS_TOKEN = window.sessionStorage.getItem('accessToken');
  const INITIAL_USER_FORM_DATA = {
    username: '',
    email: '',
    password: '',
    phone: '',
    address: '',
  };
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [isAgency, setIsAgency] = useState(false);
  const [user, setUser] = useState<LoginForm>(INITIAL_USER_FORM_DATA);
  const [error, setError] = useState('');
  const toggleAuthPages = () => {
    if (isSignIn) {
      navigate('/register');
    } else {
      navigate('/login');
    }
  };

  const onChangeEvent = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setUser((prev) => ({ ...prev, [name]: new String(value) }));
  };
  const loginHandler = async (isAgency: boolean) => {
    try {
      const response = await axios.post<AuthResponse>(
        `${isAgency ? 'agency' : 'user'}/login`,
        {
          email: user.email,
          password: user.password,
        }
      );
      dispatch(
        updateUserInfo({
          name: response.data.name,
          email: response.data.email,
          loading: false,
          error: false,
          err: null,
        })
      );
      sessionStorage.setItem('accessToken', response.data.accessToken);
      navigate('/');
    } catch (error) {
      const err = error as MyAxiosError;
      setError(err.response.data.message);
    }
  };
  const registerHandler = async (isAgency: boolean) => {
    let data: AgencyRegister | UserRegister;
    data = {
      name: user.username,
      email: user.email,
      password: user.password,
    };
    if (isAgency) {
      data = { ...data, address: user.address, phone: user.phone };
    }
    try {
      const response = await axios.post(
        `${isAgency ? 'agency' : 'user'}/create`,
        { ...data }
      );
      if (response.status === 201) navigate('/login');
    } catch (error) {
      const err = error as MyAxiosError;
      setError(err.response.data.message);
    }
  };
  const onSubmitHandler = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setError('');
    if (isSignIn) {
      await loginHandler(isAgency);
    } else {
      await registerHandler(isAgency);
    }
  };
  useEffect(() => {
    setError('');
    return () => {
      setError('');
    };
  }, [user]);

  if (ACCESS_TOKEN) return <Navigate to={'/'} />;
  return (
    <Card className="my-auto mx-auto w-50">
      <CardBody>
        <CardText>{isSignIn ? 'LOGIN' : 'SIGNUP'}</CardText>
        <ButtonGroup className="my-2">
          <Button
            color="info"
            outline={isAgency}
            onClick={() => setIsAgency(false)}
          >
            User
          </Button>
          <Button
            color="primary"
            outline={!isAgency}
            onClick={() => setIsAgency(true)}
          >
            Travel Agency
          </Button>
        </ButtonGroup>
        {error && <Alert color="danger">{error}</Alert>}
        <Form
          onSubmit={onSubmitHandler}
          style={{ justifyContent: 'flex-start' }}
        >
          {!isSignIn && (
            <FormGroup>
              <Input
                id="username"
                name="username"
                minLength={4}
                required
                placeholder="Username"
                type="text"
                value={user.username.valueOf()}
                onChange={onChangeEvent}
              />
            </FormGroup>
          )}
          <FormGroup>
            <Input
              id="email"
              name="email"
              type="email"
              required
              placeholder="Email"
              value={user.email.valueOf()}
              onChange={onChangeEvent}
            />
          </FormGroup>
          <FormGroup>
            <Input
              id="password"
              name="password"
              placeholder="Password"
              type="password"
              minLength={8}
              required
              value={user.password.valueOf()}
              onChange={onChangeEvent}
            />
          </FormGroup>
          {isAgency &&
            !isSignIn && [
              <FormGroup>
                <Input
                  id="phone"
                  name="phone"
                  placeholder="Phone"
                  type="number"
                  minLength={10}
                  required
                  value={user.phone.valueOf()}
                  onChange={onChangeEvent}
                />
              </FormGroup>,
              <FormGroup>
                <Input
                  id="address"
                  name="address"
                  placeholder="Address"
                  minLength={6}
                  type="text"
                  required
                  value={user.address.valueOf()}
                  onChange={onChangeEvent}
                />
              </FormGroup>,
            ]}
          <Button color="primary">Submit</Button>
        </Form>
        <p
          role="button"
          className="m-2 fs-6 text-primary border-bottom border-primary"
          onClick={toggleAuthPages}
        >
          {isSignIn ? 'New ' : 'Existing '}
          {isAgency ? 'Agency ?' : 'User ?'}
        </p>
      </CardBody>
    </Card>
  );
};

export default Login;
