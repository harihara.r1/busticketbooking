import { Container, Row, Col } from 'reactstrap';
interface Props {
  msg?: string;
}
const NotFoundPage = (props: Props) => {
  return (
    <Container>
      <Row>
        <Col>
          <h1>404 - Page Not Found</h1>
          <p>{props.msg || 'The requested page could not be found.'}</p>
        </Col>
      </Row>
    </Container>
  );
};

export default NotFoundPage;
