/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { useEffect } from 'react';
import { Card, Row, Col, Button } from 'reactstrap';
import { useNavigate } from 'react-router-dom';
import { MyAxiosError } from '../types/axios';
import { useDispatch } from 'react-redux';
import { clearTrips } from '../redux/tripSlice';
import { clearUserInfo } from '../redux/userSlice';
interface Props {
  error?: Error | MyAxiosError;
  title?: string;
  redirectBtnTitle?: string;
  redirectPath?: string;
  img?: string;
  resetErrorBoundary: any;
  isAuthErr?: boolean;
}
const ErrorPage = (props: Props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const redirectTo = () => {
    props.resetErrorBoundary();
    navigate(props.redirectPath || '/', { replace: true });
    return;
  };
  let errorMsg = '';
  errorMsg = props.error?.message || 'Something went wrong';
  useEffect(() => {
    dispatch(clearTrips());
    if (props.isAuthErr) dispatch(clearUserInfo());
  }, []);

  return (
    <Card className="my-auto mx-auto w-50 p-4">
      <Row>
        <Col sm="12">
          <img src={props.img} style={{ width: '10rem' }} />
        </Col>
        <Col sm="12">
          <h1>{props.title || 'Oops !!!'}</h1>

          <p className="text-center">{errorMsg}</p>
        </Col>
        <Col sm="12">
          <Button color="primary" onClick={redirectTo}>
            {props.redirectBtnTitle || 'Go Back To Home'}
          </Button>
        </Col>
      </Row>
    </Card>
  );
};

export default ErrorPage;
