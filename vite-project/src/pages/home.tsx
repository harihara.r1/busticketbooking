/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { FallbackProps, withErrorBoundary } from 'react-error-boundary';
import SearchBar from '../components/Searchbar/Searchbar';
import TripsContainer from '../components/TripContainer/TripsContainer';
import { ERR_UNAUTHORIZED, ERR_SESSION_EXPIRED } from '../constants/common';
import ErrorPage from './error';
import tripImgErr from '../assets/network.png';

const Home = () => {
  return (
    <div>
      <SearchBar />
      <TripsContainer />
    </div>
  );
};

export const HomeWithErrorBoundary = withErrorBoundary(Home, {
  fallbackRender: (props: FallbackProps) => {
    if (
      props.error.message === ERR_UNAUTHORIZED ||
      props.error.message === ERR_SESSION_EXPIRED
    ) {
      // Bubble to Auth Err boundary
      throw props.error;
    }
    return (
      <ErrorPage
        error={props.error}
        redirectBtnTitle="Go Home"
        redirectPath="/"
        img={tripImgErr}
        resetErrorBoundary={props.resetErrorBoundary}
      />
    );
  },
});
export default HomeWithErrorBoundary;
