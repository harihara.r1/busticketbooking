import TripService from "./trip.service.js";
import { connect, disconnect } from "../Mongoose/test.service.js";

beforeAll(async () => {
  await connect();
});
afterAll(async () => {
  await disconnect();
});

describe("Trip", () => {
  const DUMMY_AGENCY_ID = "64af9d57ef8671001b05ace4";
  const DUMMY_SEAT_CAPACITY = 30;
  const DUMMY_TRIP = {
    agency_id: DUMMY_AGENCY_ID,
    ac: true,
    seats_available: DUMMY_SEAT_CAPACITY,
    total_seats: DUMMY_SEAT_CAPACITY,
    from: "Madurai",
    to: "Chennai",
    departure_time: "23:00",
    duration: 8,
    date_range: ["2023-07-20"],
    price: 800
  };
  let trip_id = "";
  describe("Create Trip", () => {
    it("create new trip", async () => {
      const trips = await TripService.createTrips(DUMMY_TRIP);
      trip_id = trips[0]._id.toString();
      expect(trips.length).toBe(1);
      expect(trips[0]).toHaveProperty("available_seat_numbers");
      expect(trips[0].available_seat_numbers).toHaveLength(DUMMY_SEAT_CAPACITY);
      expect(trips[0]).toHaveProperty("departure_dateTime");
    });
    it("create invalid trip - [ seats_available > total_seats ]", async () => {
      let invalid_trip = { ...DUMMY_TRIP };
      delete invalid_trip.ac; // Deleting ac field
      await TripService.createTrips(invalid_trip).catch((err) => {
        expect(err.message).toBe("Trip validation failed: ac: ac required");
      });
    });
  });
  describe("Search Trip", () => {
    it("search for existing trip", async () => {
      const trips = await TripService.searchTrips(
        DUMMY_TRIP.from,
        DUMMY_TRIP.to,
        DUMMY_TRIP.date_range[0]
      );
      expect(trips.length).toBe(1);
      expect(trips[0]).toHaveProperty("_id");
      expect(trips[0]).toHaveProperty("available_seat_numbers");
      expect(trips[0].available_seat_numbers).toHaveLength(30);
      expect(trips[0]).toHaveProperty("departure_dateTime");
    });
    it("search for non existing trip", async () => {
      const trips = await TripService.searchTrips(
        DUMMY_TRIP.from + "NEW",
        DUMMY_TRIP.to + "NEW",
        DUMMY_TRIP.date_range[0]
      );
      expect(trips.length).toBe(0);
    });
  });
  describe("Get Agency Trip", () => {
    it("get for trips for existing agency", async () => {
      const trips = await TripService.getTripByAgencyId(DUMMY_AGENCY_ID);
      expect(trips.length).toBe(1);
    });
    it("get for trips for non existing agency", async () => {
      let non_existing_agency_id = DUMMY_AGENCY_ID.replace("a", "b");
      const trips = await TripService.getTripByAgencyId(non_existing_agency_id);
      expect(trips.length).toBe(0);
    });
  });
  describe("Get Trip by id", () => {
    it("get trip by existing trip_id", async () => {
      const trip = await TripService.getTripById(trip_id);
      expect(trip).toHaveProperty("_id");
    });
    it("get trip by non existing trip_id", async () => {
      let non_existing_trip_id = trip_id.replace(trip_id[0], "b");
      const trip = await TripService.getTripById(non_existing_trip_id).catch(
        (err) => {
          expect(err.message).toBe("Trip not found");
        }
      );
      expect(trip).toBe(undefined);
    });
  });
});
