import { ObjectId } from "mongodb";
import {
  createTrips,
  findTripByAgencyId,
  findTripById,
  searchTrips
} from "./trip.db.js";
import CustomError from "../utils/CustomError.js";

export default {
  createTrips: async ({
    agency_id,
    ac,
    seats_available,
    total_seats,
    from,
    to,
    departure_time,
    duration,
    date_range,
    price
  }) => {
    let hh = departure_time.split(":")[0];
    let mm = departure_time.split(":")[1];
    let tripList = date_range.map((date) => {
      let yyyy = +date.split("-")[0];
      let MM = +date.split("-")[1] - 1; //Sub by 1 - monthIndex
      let dd = +date.split("-")[2];
      let newDate = new Date(yyyy, MM, dd, hh, mm);
      let newTrip = {
        travels_id: new ObjectId(agency_id),
        ac,
        seats_available,
        available_seat_numbers: Array.from(
          { length: seats_available },
          (_, i) => i + 1
        ),
        total_seats,
        from,
        to,
        duration,
        price,
        departure_dateTime: newDate
      };
      return newTrip;
    });
    const response = await createTrips(tripList);
    return response;
  },
  getTripById: async (trip_id) => {
    const trip = await findTripById(trip_id);
    if (trip === null) {
      const err = new CustomError("Trip not found", 400);
      throw err;
    }
    return trip;
  },
  searchTrips: async (src, dst, date) => {
    const trips = await searchTrips(src, dst, date);
    return trips;
  },
  getTripByAgencyId: async (agency_id) => {
    const trips = await findTripByAgencyId(agency_id);
    return trips;
  }
};
