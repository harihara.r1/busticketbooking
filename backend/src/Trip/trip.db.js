import { ObjectId } from "mongodb";
import Trip from "./trip.model.js";
import dbErrorHandler from "../utils/dbErrorHandler.js";

export const searchTrips = dbErrorHandler(async (from, to, date) => {
  let nextDate = new Date(date);
  nextDate = nextDate.setDate(nextDate.getDate() + 1);
  let busList = await Trip.find({
    from: from,
    to: to,
    departure_dateTime: {
      $gte: new Date(date),
      $lte: new Date(nextDate)
    }
  });
  return busList;
});

export const findTripById = dbErrorHandler(
  async (trip_id, { projection = {}, options = {} } = {}) => {
    const trip = await Trip.findById(
      trip_id,
      { ...projection },
      { ...options }
    ).lean();
    return trip;
  }
);

export const findTripByAgencyId = dbErrorHandler(async (agency_id) => {
  const result = await Trip.find({ travels_id: new ObjectId(agency_id) });
  return result;
});

export const createTrips = dbErrorHandler(async (tripList) => {
  const response = await Trip.insertMany([...tripList]);
  return response;
});
export const updateTrip = dbErrorHandler(
  async (updatedTrip, { conditions = {}, options = {} } = {}) => {
    await Trip.updateOne(
      { ...conditions },
      { $set: updatedTrip },
      { ...options }
    ).lean();
    return updatedTrip;
  }
);
