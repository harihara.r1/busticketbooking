import joi from "joi";
import Validator from "../middlewares/Validator.js";

const searchTripSchema = joi.object({
  src: joi.string().required().messages({
    "string.base": "src must be text",
    "any.required": "src required"
  }),
  dst: joi.string().required().messages({
    "string.base": "dst must be text",
    "any.required": "dst required"
  }),
  date: joi
    .string()
    .regex(/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/)
    .required()
    .messages({
      "string.empty": "Date is empty",
      "string.pattern.base": "Please enter date in YYYY-MM-DD",
      "any.required": "Date required"
    })
});

const createTripSchema = joi.object({
  ac: joi
    .bool()
    .required()
    .messages({ "any.required": "ac must have true or false" }),
  total_seats: joi.number().min(1).max(30).required().messages({
    "number.max": "total_seats - Maximum 30 seat is allowed",
    "number.min": "total_seats - Minimum 1 seat is required",
    "number.base": "total_seats should be number",
    "any.required": "total_seats required"
  }),
  seats_available: joi.number().min(1).max(joi.ref("total_seats")).messages({
    "number.max": "seats_available - should not exceed total_seats",
    "number.min": "seats_available - Minimum 1 seat is required",
    "number.base": "total_seats should be number",
    "any.required": "total_seats required"
  }),
  from: joi.string().required().messages({
    "string.base": "from must be text",
    "any.required": "from required"
  }),
  to: joi.string().required().messages({
    "string.base": "to must be text",
    "any.required": "to required"
  }),
  departure_time: joi
    .string()
    .pattern(new RegExp("^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$"))
    .required()
    .messages({
      "string.base": "departure_time must match HH:MM format",
      "string.pattern.base": "departure_time must match HH:MM format",
      "any.required": "departure_time required"
    }),
  duration: joi.number().min(1).max(12).required().messages({
    "number.base": "duration should be number",
    "number.max": "duration - Maximum 12 hours is allowed",
    "number.min": "duration - Minimum 1 hour is required",
    "any.required": "duration required"
  }),
  price: joi.number().min(100).max(3000).required().messages({
    "number.base": "price should be number",
    "number.max": "price - Maximum 3000rs is allowed",
    "number.min": "price - Minimum 100rs is required",
    "any.required": "price required"
  }),
  date_range: joi
    .array()
    .items(
      joi
        .string()
        .regex(/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/)
        .required()
    )
    .min(1)
    .required()
    .messages({
      "array.base": "Array of dates is required",
      "string.pattern.base": "date_range should be in [YYYY-MM-DD , ...] format"
    })
});
const tripIdSchema = joi.object({
  trip_id: joi.string().alphanum().length(24).required().messages({
    "string.base": "trip_id must be string",
    "string.length": "Invalid trip_id",
    "any.required": "trip_id params required"
  })
});

const Validators = {
  search: searchTripSchema,
  newTrip: createTripSchema,
  trip: tripIdSchema
};

export default Validator(Validators);
