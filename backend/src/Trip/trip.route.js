import { Router } from "express";
import {
  getTrip,
  getTrips,
  addTrip,
  getAgencyTrips
} from "./trip.controller.js";
import Validator from "./trip.validator.js";
import { verifyUser, verifyAgency } from "../middlewares/verifyToken.js";

const router = Router();
router.get("/", Validator("search", "query"), verifyUser, getTrips);
router.get("/agency-trips", verifyAgency, getAgencyTrips);
router.post("/add", Validator("newTrip"), verifyAgency, addTrip);
router.get("/:trip_id", Validator("trip", "params"), getTrip);

export default router;
