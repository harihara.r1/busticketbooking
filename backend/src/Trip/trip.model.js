import { Schema, model } from "mongoose";

const seat_available_validator = (value) => {
  return value >= 0;
};
const total_seats_validator = (value) => {
  return value >= 10 && value <= 30;
};
const price_validator = (value) => {
  return value >= 100 && value <= 2000;
};

let tripSchema = new Schema({
  travels_id: {
    type: Schema.Types.ObjectId,
    ref: "Agency"
  },
  ac: {
    type: Boolean,
    required: [true, "ac required"]
  },
  available_seat_numbers: {
    type: [Number],
    required: [true, "available_seat_numbers required"]
  },
  seats_available: {
    type: Number,
    required: [true, "seats_available required"],
    validate: [seat_available_validator, "Sorry seats already taken"],
    min: [0, "Minimum value must be greater than 0"]
  },
  total_seats: {
    type: Number,
    required: [true, "total_seats required"],
    validate: [total_seats_validator, "Seat count should range from 10 to 30"],
    min: [10, "Minimum 10 tickets should be available"]
  },
  from: {
    type: String,
    required: [true, "from required"]
  },
  to: {
    type: String,
    required: [true, "to required"]
  },
  departure_dateTime: {
    type: Date,
    required: [true, "departure_dateTime required"]
  },
  duration: {
    type: Number,
    required: [true, "duration required"]
  },
  price: {
    type: Number,
    required: [true, "price required"],
    validate: [price_validator, "Outside the range [100 - 2000]"],
    min: [1, "Price must be above 1"]
  }
});

export default model("Trip", tripSchema);
