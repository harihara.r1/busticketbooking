import asyncErrorHandler from "../utils/asyncErrorHandler.js";
import TripService from "./trip.service.js";
import { searchTrips, findTripByAgencyId } from "./trip.db.js";

//Get Bus List
export const getTrips = asyncErrorHandler(async (req, res, next) => {
  const { src, dst, date } = req.query;
  let busList = await TripService.searchTrips(src, dst, date);
  return res.send({ trips: busList });
});

// Get Specific trip
export const getTrip = asyncErrorHandler(async (req, res, next) => {
  const trip_id = req.params["trip_id"];
  const trip = await TripService.getTripById(trip_id);
  return res.send(trip);
});

// Get trips by agency id
export const getAgencyTrips = asyncErrorHandler(async (req, res, next) => {
  const agency_id = req["agency_id"];
  const result = await TripService.getTripByAgencyId(agency_id);
  return res.send({ trips: result });
});

//Adding new trips
export const addTrip = asyncErrorHandler(async (req, res, next) => {
  const {
    ac,
    seats_available,
    total_seats,
    from,
    to,
    departure_time,
    duration,
    date_range,
    price
  } = req.body;
  const agency_id = req["agency_id"];
  const response = await TripService.createTrips({
    agency_id,
    ac,
    seats_available,
    total_seats,
    from,
    to,
    departure_time,
    duration,
    date_range,
    price
  });
  res.status(201).send({ trips: response });
});
