import CustomError from "./CustomError.js";

export default (dbCall) => {
  return async (...arg) => {
    try {
      return await dbCall(...arg);
    } catch (err) {
      if (err.name === "ValidationError") {
        throw new CustomError(err.message);
      }
      if (err.name === "MongoServerError") {
        if (err.code === 11000)
          throw new CustomError(
            JSON.stringify(err.keyValue) + " Already found"
          );
        throw new CustomError("Error in reaching DB");
      }
      throw new CustomError("Error in reaching DB");
    }
  };
};
