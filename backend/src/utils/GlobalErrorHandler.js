const devErrors = (res, error) => {
  return res.status(error.statusCode).json({
    status: error.status,
    message: error.message,
    stackTrace: error.stack,
    error: error
  });
};

const prodErrors = (res, error) => {
  if (error.isOperational) {
    return res.status(error.statusCode).json({
      status: error.status,
      message: error.message
    });
  } else {
    return res.status(500).json({
      status: "fail",
      message: "Something went wrong! Try again later"
    });
  }
};

export default (error, req, res, next) => {
  if (error.name === "TokenExpiredError") {
    error.statusCode = 401;
  } else {
    error.statusCode = error.statusCode || 500;
    error.status = error.status || "error";
  }
  if (process.env.NODE_ENV === "production") {
    return prodErrors(res, error);
  } else if (process.env.NODE_ENV === "development") {
    return devErrors(res, error);
  }
};
