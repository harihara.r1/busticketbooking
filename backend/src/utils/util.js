const isNotEmpty = (word) => {
  if (word === undefined) return false;
  let a = word.toString().trim();
  return a.length > 0;
};

exports.notEmpty = isNotEmpty;
