import { Router } from "express";
import {
  createAgency,
  loginAgency,
  getAgencyInfo
} from "./agency.controller.js";
import Validator from "./agency.validator.js";
import { verifyAgency, verifyToken } from "../middlewares/verifyToken.js";

const router = Router();

router.post("/create", Validator("register"), createAgency);
router.post("/login", Validator("login"), loginAgency);
router.get("/get-info", verifyToken, verifyAgency, getAgencyInfo);

export default router;
