import joi from "joi";
import Validator from "../middlewares/Validator.js";

const invalidPasswordMessages = {
  "string.empty": "Password is empty",
  "string.min": "Password must have minimum 8 characters",
  "any.required": "Password required"
};
const invalidEmailMessages = {
  "string.empty": "Email is empty",
  "string.email": "Invalid email",
  "any.required": "Email required"
};

const loginSchema = joi.object({
  email: joi
    .string()
    .email()
    .lowercase()
    .required()
    .messages({ ...invalidEmailMessages }),
  password: joi
    .string()
    .min(8)
    .required()
    .messages({ ...invalidPasswordMessages })
});
const registerSchema = joi.object({
  name: joi.string().min(4).required().messages({
    "string.min": "Agency name must have minimum 4 characters",
    "any.required": "Agency name required"
  }),
  email: joi
    .string()
    .email()
    .lowercase()
    .required()
    .messages({ ...invalidEmailMessages }),
  password: joi
    .string()
    .min(8)
    .required()
    .messages({ ...invalidPasswordMessages }),
  address: joi.string().min(6).required().messages({
    "string.min": "Address must have minimum 6 characters",
    "any.required": "Address required"
  }),
  phone: joi.string().length(10).required().messages({
    "string.length": "10 digit phone number required",
    "any.required": "phone required"
  })
});

const Validators = {
  login: loginSchema,
  register: registerSchema
};

export default Validator(Validators);
