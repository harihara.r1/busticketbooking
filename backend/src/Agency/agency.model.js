import { Schema, model } from "mongoose";

let agencySchema = new Schema({
  name: {
    type: String,
    required: [true, "Name field required"],
    unique: [true, "Travels name already taken"]
  },
  email: {
    type: String,
    required: [true, "Email field required"],
    unique: [true, "Account already exists"]
  },
  password: {
    type: String,
    minLength: [8, "Minimum 8 characters required"],
    required: [true, "Password field required"]
  },
  adminAccess: {
    type: Boolean,
    required: [true, "adminAccess field required"]
  },
  emailVerified: {
    type: Boolean,
    required: [true, "emailVerified field required"]
  },
  address: {
    type: String,
    required: [true, "Address field required"]
  },
  phone: {
    type: Number,
    required: [true, "Phone field required"]
  }
});

export default model["Agency"] || model("Agency", agencySchema);
