import AgencyService from "./agency.service.js";
import { connect, disconnect } from "../Mongoose/test.service.js";

beforeAll(async () => {
  await connect();
});
afterAll(async () => {
  await disconnect();
});
describe("Agency", () => {
  let agency_id;
  describe("Register", () => {
    it("register new agency", async () => {
      const response = await AgencyService.createAgency(
        "test-agency",
        "testagency@gmail.com",
        "test12345",
        "test-address",
        "1234567890"
      );
      agency_id = response._id;
      expect(response).toHaveProperty("_id");
      expect(response.email).toBe("testagency@gmail.com");
      expect(response.emailVerified).toBe(false);
      expect(response).toHaveProperty("password");
      expect(response.name).toBe("test-agency");
      expect(response.phone).toBe(1234567890);
      expect(response.address).toBe("test-address");
    });
  });
  describe("Login", () => {
    it("login existing agency", async () => {
      const accessToken = await AgencyService.loginAgency(
        "testagency@gmail.com",
        "test12345"
      );
      expect(accessToken).toHaveProperty("accessToken");
    });
    it("login existing user with wrong credentials", async () => {
      const agency = await AgencyService.loginAgency(
        "testagency@gmail.com",
        "WRONG_PASSWORD"
      ).catch((err) => {
        expect(err.message).toBe("Wrong Credentials");
      });
      expect(agency).toBe(undefined);
    });
    it("login new user", async () => {
      const agency = await AgencyService.loginAgency(
        "newagency@gmail.com",
        "NEW_PASSWORD"
      ).catch((err) => {
        expect(err.message).toBe("Agency not found");
      });
      expect(agency).toBe(undefined);
    });
  });
  describe("Get Agency Info", () => {
    it("get already existing agency info", async () => {
      const agencyDetails = await AgencyService.getAgencyInfo(agency_id);
      expect(agencyDetails.name).toBe("test-agency");
      expect(agencyDetails.email).toBe("testagency@gmail.com");
      expect(agencyDetails).toHaveProperty("emailVerified");
      expect(agencyDetails).toHaveProperty("adminAccess");
      expect(agencyDetails.isAgency).toBe(true);
    });
  });
});
