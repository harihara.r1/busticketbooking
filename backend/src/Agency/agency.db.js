import Agency from "./agency.model.js";
import dbErrorHandler from "../utils/dbErrorHandler.js";

export const createNewAgency = dbErrorHandler(
  async (name, email, password, address, phone) => {
    const agency = new Agency({
      name: name,
      email: email,
      password: password,
      adminAccess: false,
      emailVerified: false,
      address: address,
      phone: phone
    });
    const response = await agency.save();
    return response;
  }
);

export const findAgencyByEmail = dbErrorHandler(async (email) => {
  const agency = await Agency.findOne({ email: email }).lean();
  return agency;
});

export const findAgencyById = dbErrorHandler(async (id) => {
  const agency = await Agency.findById(id).lean();
  return agency;
});
