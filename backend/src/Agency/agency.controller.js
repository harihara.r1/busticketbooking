import asyncErrorHandler from "../utils/asyncErrorHandler.js";
import AgencyService from "./agency.service.js";

// Create travel agency
export const createAgency = asyncErrorHandler(async (req, res, next) => {
  const { name, email, password, address, phone } = req.body;
  await AgencyService.createAgency(name, email, password, address, phone);
  return res.status(201).send("Account created successfully");
});

//Login Agency
export const loginAgency = asyncErrorHandler(async (req, res, next) => {
  const { email, password } = req.body;
  const accessToken = await AgencyService.loginAgency(email, password);
  return res.send(accessToken);
});

// Get currently logged in agency details
export const getAgencyInfo = asyncErrorHandler(async (req, res, next) => {
  const agency_id = req["agency_id"];
  const agency = await AgencyService.getAgencyInfo(agency_id);
  return res.send({ ...agency });
});
