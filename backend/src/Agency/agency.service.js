import CustomError from "../utils/CustomError.js";
import jwt from "jsonwebtoken";
import { hash, compare } from "bcrypt";
import {
  createNewAgency,
  findAgencyByEmail,
  findAgencyById
} from "./agency.db.js";

export default {
  createAgency: async (name, email, password, address, phone) => {
    const hashedPassword = await hash(password, 10);
    return await createNewAgency(name, email, hashedPassword, address, phone);
  },
  loginAgency: async (email, password) => {
    const existingAgency = await findAgencyByEmail(email);
    if (existingAgency === null) {
      throw new CustomError("Agency not found", 401);
    }
    const passwordMatch = await compare(password, existingAgency["password"]);
    if (!passwordMatch) {
      throw new CustomError("Wrong Credentials", 401);
    }
    let data = {
      isAgency: true,
      user_id: existingAgency["_id"]
    };
    const token = jwt.sign(data, "key", {
      expiresIn: "2h"
    });
    return { accessToken: token };
  },
  getAgencyInfo: async (agency_id) => {
    const agency = await findAgencyById(agency_id);
    if (agency === null) {
      throw new CustomError("Agency not found", 400);
    }
    return {
      name: agency.name,
      email: agency.email,
      emailVerified: agency.emailVerified,
      adminAccess: agency.adminAccess,
      isAgency: true
    };
  }
};
