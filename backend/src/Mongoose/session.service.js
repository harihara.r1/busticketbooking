import { startSession } from "mongoose";

class SessionService {
  constructor() {
    this.session = null;
  }
  async startSession() {
    this.session = await startSession();
  }
  async endSession() {
    await this.session.endSession();
  }
  async withTransaction(fn, options) {
    return await this.session.withTransaction(
      async () => {
        return await fn(this.session);
      },
      { ...options }
    );
  }
}
export default SessionService;
