import jwt from "jsonwebtoken";
import CustomError from "../utils/CustomError.js";

// Verify whether the token is valid
export const verifyToken = (req, res, next) => {
  const token = req.headers.authorization;
  if (!token) {
    const err = new CustomError("Token not found", 401);
    return next(err);
  }
  const bearer = token.split(" ")[0];
  if (!bearer || bearer.toString().toLowerCase() !== "bearer") {
    const err = new CustomError("Bearer token needed", 401);
    return next(err);
  }
  const jwtToken = token.split(" ")[1];
  let verified;
  try {
    verified = jwt.verify(jwtToken, "key");
  } catch (err) {
    if (err.name === "TokenExpiredError")
      throw new CustomError("Session expired", 401);
    else throw new CustomError("Access denied", 401);
  }

  if (verified?.isAgency === true) req.agency_id = verified.user_id;
  else {
    req.user_id = verified.user_id;
    req.name = verified.name;
    req.email = verified.email;
  }

  next();
};

// Allow only agency
export const verifyAgency = (req, res, next) => {
  if (req.agency_id !== null) next();
  else {
    const err = new CustomError("You don't have access", 403);
    next(err);
  }
};
// Allow only users
export const verifyUser = (req, res, next) => {
  if (req.user_id !== null) next();
  else {
    const err = new CustomError("You don't have access", 403);
    next(err);
  }
};
