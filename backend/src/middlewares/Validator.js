import asyncErrorHanlder from "../utils/asyncErrorHandler.js";
import CustomError from "../utils/CustomError.js";

export default (Validators) => {
  return (validator, property = "body") => {
    return asyncErrorHanlder(async (req, res, next) => {
      const validated = await Validators[validator].validateAsync(
        req[property]
      );
      req.body = validated;
      next();
    });
  };
};
