import express from "express";
import cors from "cors";
import { mongoConnect } from "./Mongoose/connect.service.js";
import { verifyToken } from "./middlewares/verifyToken.js";
import userRoutes from "./User/user.route.js";
import agencyRoutes from "./Agency/agency.route.js";
import tripRoutes from "./Trip/trip.route.js";
import bookingRoutes from "./Booking/booking.route.js";
import GlobalErrorHandler from "./utils/GlobalErrorHandler.js";
import CustomError from "./utils/CustomError.js";

const app = express();

// Handling CORS
const corsOption = {
  origin: true,
  credentials: true
};
app.use(cors(corsOption));

// Parsing Request
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Updated Routes
app.use("/user", userRoutes);
app.use("/agency", agencyRoutes);
app.use("/trip", verifyToken, tripRoutes);
app.use("/booking", verifyToken, bookingRoutes);

// Welcome API
app.get("/", (req, res, next) => {
  res.send("Hello from Bus Ticketing Backend");
});

// Handling 404 routes
app.all("*", (req, res, next) => {
  const err = new CustomError(
    `Can't find ${req.originalUrl} on the server!`,
    404
  );
  next(err);
});

// Global Error Handling
app.use(GlobalErrorHandler);

// Start server once mongo est. conn
if (process.env.NODE_ENV === "development") {
  console.log("Preparing to start");
  console.log(process.env.DB_CONNECTION_URL);
  await mongoConnect(() => {
    app.listen(3000, () => {
      console.log("Server started at PORT 3000");
    });
  });
}

export default app;
