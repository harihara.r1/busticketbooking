import Booking from "./booking.model.js";
import dbErrorHandler from "../utils/dbErrorHandler.js";
import mongoose from "mongoose";
import { ObjectId } from "mongodb";
export const createBooking = dbErrorHandler(
  async (
    trip_id,
    user_id,
    seat_numbers,
    count,
    total_price,
    { options = {} }
  ) => {
    const booking = new Booking({
      trip_id,
      user_id,
      booked_seat_numbers: seat_numbers,
      count,
      total_price
    });
    let response = await booking.save({ ...options });
    return response[""];
  }
);

export const findBooking = dbErrorHandler(
  async ({ conditions = {}, projection = {}, options = {} } = {}) => {
    const response = await Booking.find(
      { ...conditions },
      { ...projection },
      { ...options }
    );
    return response;
  }
);

export const getBookingWithAgency = dbErrorHandler(async (user_id) => {
  try {
    const booking = await Booking.aggregate([
      {
        $match: { user_id: new ObjectId(user_id) }
      },
      {
        $lookup: {
          from: "trips",
          localField: "trip_id",
          foreignField: "_id",
          as: "trip"
        }
      },
      {
        $unwind: "$trip"
      },
      {
        $lookup: {
          from: "agencies",
          localField: "trip.travels_id",
          foreignField: "_id",
          as: "agency"
        }
      },
      {
        $unwind: "$agency"
      },
      {
        $project: {
          _id: 1,
          count: 1,
          booked_seat_numbers: 1,
          total_price: 1,
          booked_date: 1,
          "trip.duration": 1,
          "trip.from": 1,
          "trip.to": 1,
          "trip.departure_dateTime": 1,
          "agency.name": 1
        }
      }
    ]);
    return booking; // Since we are using _id in the $match stage, we expect a single result
  } catch (err) {
    error(err);
    throw err;
  }
});

export const deleteBooking = dbErrorHandler(
  async ({ conditions = {}, options = {} } = {}) => {
    await Booking.deleteOne({ ...conditions }, { ...options });
  }
);
