import joi from "joi";
import Validator from "../middlewares/Validator.js";

const bookTripSchema = joi.object({
  trip_id: joi.string().alphanum().length(24).required(),
  count: joi.number().min(1).max(5).required(),
  seat_numbers: joi
    .array()
    .items(joi.number())
    .length(joi.ref("count"))
    .required(),
  total_price: joi.number().min(100).max(15000).required()
});

const validators = {
  bookTrip: bookTripSchema
};

export default Validator(validators);
