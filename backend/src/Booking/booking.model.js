import { Schema, model } from "mongoose";

let bookingSchema = new Schema({
  trip_id: {
    type: Schema.Types.ObjectId,
    ref: "Trip",
    required: true
  },
  user_id: {
    type: Schema.Types.ObjectId,
    required: true
  },
  count: {
    type: Number,
    required: true
  },
  booked_seat_numbers: {
    type: [Number],
    required: true
  },
  total_price: {
    type: Number,
    required: true
  },
  booked_date: {
    type: Date,
    default: Date.now()
  }
});

export default model("Booking", bookingSchema);
