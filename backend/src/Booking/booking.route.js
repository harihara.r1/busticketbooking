import { Router } from "express";
import { bookTrip, getUserBookings } from "./booking.controller.js";
import Validator from "./booking.validator.js";
import { verifyUser, verifyAgency } from "../middlewares/verifyToken.js";

const router = Router();
router
  .post("/", Validator("bookTrip"), verifyUser, bookTrip)
  .get("/", verifyUser, getUserBookings);

export default router;
