import asyncErrorHandler from "../utils/asyncErrorHandler.js";
import BookingService from "./booking.service.js";

// Book trip
export const bookTrip = asyncErrorHandler(async (req, res, next) => {
  const { trip_id, count, seat_numbers, total_price } = req.body;
  const user_id = req["user_id"];
  const booking = await BookingService.bookTrip(
    trip_id,
    user_id,
    count,
    seat_numbers,
    total_price
  );
  return res.send({ booking: booking });
});

// Get user booking
export const getUserBookings = async (req, res, next) => {
  const user_id = req["user_id"];
  const response = await BookingService.getUserBookings(user_id);
  return res.send({ bookings: response });
};
