import MongooseService from "../Mongoose/session.service.js";
import { findTripById, updateTrip } from "../Trip/trip.db.js";
import CustomError from "../utils/CustomError.js";
import { ObjectId } from "mongodb";
import {
  createBooking,
  findBooking,
  getBookingWithAgency
} from "../Booking/booking.db.js";
import agencyService from "../Agency/agency.service.js";
import { findAgencyByEmail, findAgencyById } from "../Agency/agency.db.js";
const checkAvailability = (arr1, arr2) => {
  //Chk whether arr2 is subset of arr1
  let m = arr1.length;
  let n = arr2.length;
  let s = new Set();
  for (let i = 0; i < m; i++) {
    s.add(arr1[i]);
  }
  let p = s.size;
  for (let i = 0; i < n; i++) {
    s.add(arr2[i]);
  }

  if (s.size == p) return true;
  else return false;
};

const removeSeatsFromTrip = (trip, seat_numbers) => {
  let available_seat_numbers = trip["available_seat_numbers"];
  trip["available_seat_numbers"] = [...available_seat_numbers].filter(
    (i) => !seat_numbers.includes(i)
  );
  return trip;
};

export default {
  bookTrip: async (trip_id, user_id, count, seat_numbers, total_price) => {
    let newBooking = null;
    let updatedTrip = null;
    const sessionInstance = new MongooseService();
    await sessionInstance.startSession();
    await sessionInstance.withTransaction(
      async (session) => {
        // Step 1 - Check seat availabilty
        const oldTrip = await findTripById(trip_id, { options: session });
        if (!oldTrip) throw new CustomError("Trip not found", 400);
        if (!checkAvailability(oldTrip?.available_seat_numbers, seat_numbers))
          throw new CustomError("Seats already taken", 200);

        // Step 2 - Update seat availabilty
        let newTrip = removeSeatsFromTrip(oldTrip, seat_numbers);
        newTrip.seats_available = newTrip.seats_available - count;
        updatedTrip = await updateTrip(newTrip, {
          conditions: { _id: new ObjectId(trip_id) },
          options: { session }
        });

        // Step 3 - Create booking
        newBooking = await createBooking(
          trip_id,
          user_id,
          seat_numbers,
          count,
          total_price,
          { options: session }
        );
      },
      {
        readConcern: { level: "majority" },
        writeConcern: { w: "majority" }
      }
    );
    await sessionInstance.endSession();
    const travels = await findAgencyById(updatedTrip.travels_id.toString());

    return {
      trip_id,
      count,
      seat_numbers,
      total_price,
      dept_dateTime: updatedTrip.departure_dateTime,
      travels_name: travels.name
    };
  },
  getUserBookings: async (user_id) => {
    const response = await getBookingWithAgency(user_id);
    return response;
  }
};
