import { Schema, model } from "mongoose";

const email_validator = (value) => {
  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(value);
};

let userSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: [true, "Username required"],
    minLength: [3, "Username must have atleast 4 characters"],
    maxLength: [15, "Username must have atmost 15characters"]
  },
  email: {
    type: String,
    validate: [email_validator, "Please enter valid email"],
    required: [true, "Email field required"],
    unique: [true, "Account already exists"]
  },
  emailVerified: { type: Boolean, default: false },
  password: {
    type: String,
    required: [true, "Password field required"]
  }
});

export default model("User", userSchema);
