import User from "./user.model.js";
import dbErrorHandler from "../utils/dbErrorHandler.js";

export const createNewUser = dbErrorHandler(async (name, email, password) => {
  const user = new User({
    name: name,
    email: email,
    emailVerified: false,
    password: password
  });
  const response = await user.save();
  return response;
});

export const findUserByEmail = dbErrorHandler(async (email) => {
  const user = await User.findOne({ email: email }).lean();
  return user;
});

export const findUserById = dbErrorHandler(async (id) => {
  const user = await User.findById(id).lean();
  return user;
});
