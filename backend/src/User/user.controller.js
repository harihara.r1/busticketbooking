import UserService from "./user.service.js";
import asyncErrorHandler from "../utils/asyncErrorHandler.js";

// Register User
export const createUser = asyncErrorHandler(async (req, res, next) => {
  const { name, email, password } = req.body;
  await UserService.createUser(name, email, password);
  return res.status(201).send("User created successfully");
});

// Login user
export const loginUser = asyncErrorHandler(async (req, res, next) => {
  const { email, password } = req.body;
  const accessToken = await UserService.loginUser(email, password);
  res.status(200).send(accessToken);
});

// Get currently logged in user details
export const getUserInfo = asyncErrorHandler(async (req, res, next) => {
  const { name, email } = req;
  //const userInfo = await UserService.getUserInfo(userId);
  return res.status(200).send({
    name,
    email
  });
});
