import { Router } from "express";
import { createUser, loginUser, getUserInfo } from "./user.controller.js";
import { verifyUser, verifyToken } from "../middlewares/verifyToken.js";
import Validator from "./user.validator.js";
const router = Router();

router.post("/create", Validator("register"), createUser);
router.post("/login", Validator("login"), loginUser);
router.get("/get-info", verifyToken, verifyUser, getUserInfo);

export default router;
