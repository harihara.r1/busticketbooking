import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import { findUserByEmail, createNewUser, findUserById } from "./user.db.js";
import CustomError from "../utils/CustomError.js";

const UserService = {
  loginUser: async (email, password) => {
    const existingUser = await findUserByEmail(email);
    if (!existingUser) {
      throw new CustomError("User not found", 401);
    }
    const passwordMatch = await bcrypt.compare(password, existingUser.password);
    if (passwordMatch) {
      let data = {
        isAgency: false,
        user_id: existingUser._id,
        email: existingUser.email,
        name: existingUser.name
      };
      const token = jwt.sign(data, "key", {
        expiresIn: "2h"
      });
      return { accessToken: token, name: data.name, email: data.email };
    } else {
      throw new CustomError("Wrong Credentials", 401);
    }
  },
  createUser: async (name, email, password) => {
    const hashedPassword = await bcrypt.hash(password, 10);
    return await createNewUser(name, email, hashedPassword);
  },
  getUserInfo: async (userId) => {
    const user = await findUserById(userId);
    if (!user) {
      throw new CustomError("User not found", 400);
    }
    return {
      name: user.name,
      email: user.email,
      emailVerified: user.emailVerified
    };
  }
};

export default UserService;
