import UserService from "./user.service.js";
import { connect, disconnect } from "../Mongoose/test.service.js";

beforeAll(async () => {
  await connect();
});
afterAll(async () => {
  await disconnect();
});
describe("User", () => {
  let user_id;
  describe("Register", () => {
    it("register new user", async () => {
      const response = await UserService.createUser(
        "test-user",
        "testuser@gmail.com",
        "test12345"
      );
      user_id = response._id;
      expect(response).toHaveProperty("_id");
      expect(response.email).toBe("testuser@gmail.com");
      expect(response.emailVerified).toBe(false);
      expect(response).toHaveProperty("password");
      expect(response.name).toBe("test-user");
    });
  });
  describe("Login", () => {
    it("login existing user", async () => {
      const accessToken = await UserService.loginUser(
        "testuser@gmail.com",
        "test12345"
      );
      expect(accessToken).toHaveProperty("accessToken");
    });
    it("login existing user with wrong credentials", async () => {
      const user = await UserService.loginUser(
        "testuser@gmail.com",
        "WRONG_PASSWORD"
      ).catch((err) => {
        expect(err.message).toBe("Wrong Credentials");
      });
      expect(user).toBe(undefined);
    });
    it("login new user", async () => {
      const user = await UserService.loginUser(
        "newuser@gmail.com",
        "NEW_PASSWORD"
      ).catch((err) => {
        expect(err.message).toBe("User not found");
      });
      expect(user).toBe(undefined);
    });
  });
  describe("Get User Info", () => {
    it("get already existing user info", async () => {
      const userDetails = await UserService.getUserInfo(user_id);
      expect(userDetails.name).toBe("test-user");
      expect(userDetails.email).toBe("testuser@gmail.com");
      expect(userDetails).toHaveProperty("emailVerified");
    });
  });
});
