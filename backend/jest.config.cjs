module.exports = {
  transform: 
    {
    '^.+\\.jsx?$': 'babel-jest', // Use Babel for .js and .jsx files
  },
};
